﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
public class Service : IService {
    public string GetData(int value) {
        return string.Format("You entered: {0}", value);
    }

    public ReturnSet SendData(string data, string connStr) {
        return SaveToDb(data, connStr);
    }

    public CompositeType GetDataUsingDataContract(CompositeType composite) {
        if (composite == null) {
            throw new ArgumentNullException("composite");
        }
        if (composite.BoolValue) {
            composite.StringValue += "Suffix";
        }
        return composite;
    }


    private static ReturnSet SaveToDb(string data, string connStr) {
        var result = new ReturnSet();
        using (var connection = new SqlConnection(connStr)) {
            try {
                var command = connection.CreateCommand();
                command.CommandText = "insert into VALUELIST (VALUEX) values (@P1)";
                command.Parameters.AddWithValue("P1", data);
                connection.Open();
                var count = command.ExecuteNonQuery();
                result.BoolValue = true;
                result.StringValue = "Success";
            }
            catch (Exception ex) {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Exception : {0}", ex.Message);
                result.BoolValue = false;
                result.StringValue = ex.Message;
            }
        }
        return result;
    }

}
